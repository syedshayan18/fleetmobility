# Wunder Fleet

# Possible Performance Optimisations
### Third Party Libraries:
Third party Libraries like Moya and SDWebImage. URLSession could be used to remove third party Networking dependency and for SDWebImage alternative custom downloading images and cache mechanism would be a good option too, But it usually depends on the application requirement as we usually concentrate more on business problems rather than implementing boiler Code or reinventing the wheel having said that it is good to have custom made structures gives application more flexibility and control.

### Separate Class for Constants:
We could build separate Class for all the constants like messages, errors, titles to easily manipulate string in future.

### MapViewController Location Checks:
We could build separate file for location checks if view controller gets more code in it.

### Application Architecture and Pods Used:
1. MVVM
2. SDWebImage
3. Moya (Networking Layer)
### Advantage of Moya:
Code readability, easy to modularise, testable code clear usage of different endpoints
### Bonus Work:
1. Custom Car Pin.
2. Custom Callout View is created for user friendly UI.
3. MapViewController Refresh button is implemented in case of no data is available on
first attempt.
