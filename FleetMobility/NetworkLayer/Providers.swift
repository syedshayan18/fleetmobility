//
//  Providers.swift
//  FleetMobility
//
//  Created by Shayan Ali on 11/09/2019.
//  Copyright © 2019 Shayan Ali. All rights reserved.
//

import Foundation
import Moya

struct Providers {
    static let carProvider = MoyaProvider<CarService>(manager: RequestManager.manager())

}
