//
//  Routes.swift
//  FleetMobility
//
//  Created by Shayan Ali on 11/09/2019.
//  Copyright © 2019 Shayan Ali. All rights reserved.
//

import Foundation
struct Routes {
    
    static let baseUrl = "https://s3.eu-central-1.amazonaws.com/"
    static let postBaseUrl = "https://4i96gtjfia.execute-api.eu-central-1.amazonaws.com/"
    static let authorizationHeader = "df7c313b47b7ef87c64c0f5f5cebd6086bbb0fa"
    static let cars = "wunderfleet-recruiting-dev/cars.json"
    static let carDetails = "wunderfleet-recruiting-dev/cars"
    static let quickRentCar = "default/wunderfleet-recruiting-mobile-dev-quick-rental"
    
    
}
