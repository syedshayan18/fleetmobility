//
//  CarService.swift
//  FleetMobility
//
//  Created by Shayan Ali on 11/09/2019.
//  Copyright © 2019 Shayan Ali. All rights reserved.
//

import Foundation
import Moya

enum CarService {
    
    case getCars
    case getCarDetails(id: Int)
    case quickRentCar(carId: Int)
}

extension CarService: TargetType {
    
    var baseURL: URL { switch self {
    case .quickRentCar:
        return URL(string: Routes.postBaseUrl)!
        
    default:
        return URL(string: Routes.baseUrl)!
        }
        
        }
    
    var path: String {
        switch self {
        case .getCars:
            return Routes.cars
        case .getCarDetails(let id):
            return "\(Routes.carDetails)/\(id)"
        case .quickRentCar:
            return Routes.quickRentCar
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .getCars, .getCarDetails:
            return .get
        case .quickRentCar:
            return .post
        }
    }
    
    var task: Task {
        switch self {
        case .getCars, .getCarDetails:
            return .requestPlain
        case .quickRentCar(let carId):
            return .requestParameters(parameters: ["carId": carId], encoding: JSONEncoding.default)
            
        }
    }
    
    var headers: [String: String]? {
        
        switch self {
        case .quickRentCar:
            return ["Content-type": "application/x-www-form-urlencoded","Authorization":"Bearer \(Routes.authorizationHeader)"]
        default:
             return nil
        }
    }
    
    var sampleData: Data {
        return Data()
    }
}
