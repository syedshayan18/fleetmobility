//
//  ViewController.swift
//  FleetMobility
//
//  Created by Shayan Ali on 11/09/2019.
//  Copyright © 2019 Shayan Ali. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
class MapViewController: UIViewController {
    // MARK: Outlets
    @IBOutlet weak private var mapView: MKMapView!
    // MARK: Properties
    private let locationManager = CLLocationManager()
    private let regionInMeters: Double = 10000
    private lazy var carViewModel = CarViewModel(delegate: self)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //check for location permission and update user location
        checkLocationServices()
        //initiate car objects request
        getCarsRequest()
    }
   
    private func getCarsRequest() {
        carViewModel.getCars { [weak self] (result) in
            switch result {
            case .success(let successMessage):
                self?.showAlert(title: "Cars Available", message: successMessage)
            case .failure(let error):
                self?.showAlert(message: error)
            }
        }
    }
    //refresh data by recalling method
    @IBAction func refreshButtonPressed(_ sender: UIBarButtonItem) {
        getCarsRequest()
    }
}

//MARK: Annotation Delegate Conformation
extension MapViewController: AnnotationsDelegate {
    func getAnnotations(_ annotationsList: [CarAnnotation]?) {
        //optional binding
        guard let annotationlist = annotationsList else{return}
        //add all car pins onto MapView
        mapView.addAnnotations(annotationlist)
    }
}

//MARK: MKMapView Delegate
extension MapViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
    
        if view.annotation is MKUserLocation
        {
            // Don't proceed with custom callout
            return
        }
        //remove unselected Pins from MapView
        mapView.annotations.forEach { (annotation) in
            if !annotation.isEqual(view.annotation) {
                mapView.removeAnnotation(annotation)
            }
        }
        
        //Custom CallOut View
        let carAnnotation = view.annotation as! CarAnnotation
        let views = Bundle.main.loadNibNamed("CustomCallOutView", owner: nil, options: nil)
        let calloutView = views?[0] as! CustomCallOutView
        calloutView.titleLabel.text = carAnnotation.title == "" ? "Unknown" : carAnnotation.title
        let tapButton = UIButton(frame: calloutView.titleLabel.frame)
        tapButton.addTarget(self, action: #selector(self.titleTapped), for: .touchUpInside)
        tapButton.tag = carAnnotation.carId
        calloutView.addSubview(tapButton)
        
        //Positioning CallOut View
        calloutView.center = CGPoint(x: view.bounds.size.width / 2, y: -calloutView.bounds.size.height*0.30)
        view.addSubview(calloutView)
        //Center CallOut View on selection
        mapView.setCenter((view.annotation?.coordinate)!, animated: true)
  
    }
    //title tapping action function
    @objc func titleTapped(_ sender: UIButton) {
        //instantiating ViewController (i wrote protocol extension for easy instantiating View Controllers)
        let carDetailVC = CarDetailViewController.instantiate()
        carDetailVC.carId = sender.tag
        self.navigationController?.pushViewController(carDetailVC, animated: true)
    }
    
    func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
        mapView.addAnnotations(carViewModel.makeAnnotations(carList: self.carViewModel.carList))
        if view.isKind(of: AnnotationView.self)
        {
            for subview in view.subviews
            {
                subview.removeFromSuperview()
            }
        }
     
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        if annotation is MKUserLocation
        {
            return nil
        }
        var annotationView = self.mapView.dequeueReusableAnnotationView(withIdentifier: "Pin")
        if annotationView == nil{
            annotationView = AnnotationView(annotation: annotation, reuseIdentifier: "Pin")
            annotationView?.canShowCallout = false
        }else{
            annotationView?.annotation = annotation
        }
        annotationView?.image = UIImage(named: "carPin")
        return annotationView
    }
}

//MARK: LocationManager Delegate
extension MapViewController: CLLocationManagerDelegate {
    //update user Location
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        guard let location = locations.last else { return }
        let region = MKCoordinateRegion.init(center: location.coordinate, latitudinalMeters: regionInMeters, longitudinalMeters: regionInMeters)
        mapView.setRegion(region, animated: true)
        
    }
    //check for changes in location authorization
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        checkLocationServices()
    }
}


//MARK: Location Work
extension MapViewController {
    private func setupLocationManager() {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
    }
    
    private func checkLocationAuthorization() {
        switch CLLocationManager.authorizationStatus() {
        case .authorizedWhenInUse:
            mapView.showsUserLocation = true
            centerViewOnUserLocation()
            locationManager.startUpdatingLocation()
            
        case .denied:
            self.showAlert(title: "Location Denied", message: "Please goto settings to enable location")
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
            
        case .restricted:
            self.showAlert(title: "Location Restricted", message: "Please goto settings to enable location")
        case .authorizedAlways:
            break
            
        }
    }
    
    private func centerViewOnUserLocation() {
        if let location = locationManager.location?.coordinate {
            let region = MKCoordinateRegion.init(center: location, latitudinalMeters: regionInMeters, longitudinalMeters: regionInMeters)
            mapView.setRegion(region, animated: true)
        }
    }
    
    private func checkLocationServices() {
        if CLLocationManager.locationServicesEnabled() {
            setupLocationManager()
            checkLocationAuthorization()
        }
        else {
            self.showAlert(title: "Location Error", message: "Please goto settings to enable location")
        }
    }
}
