//
//  CarDetailViewController.swift
//  FleetMobility
//
//  Created by Shayan on 9/11/19.
//  Copyright © 2019 Shayan Ali. All rights reserved.
//

import UIKit

class CarDetailViewController: UIViewController,Storyboarded {
    
    // MARK: Outlets
    @IBOutlet weak private var carImageView: UIImageView!
    @IBOutlet weak private var titleLabel: UILabel!
    @IBOutlet weak private var cleanLabel: UILabel!
    @IBOutlet weak private var damagedLabel: UILabel!
    @IBOutlet weak private var licensePlateLabel: UILabel!
    @IBOutlet weak private var fuelLevelLabel: UILabel!
    @IBOutlet weak private var vehicleStateIdLabel: UILabel!
    @IBOutlet weak private var hardwareIdLabel: UILabel!
    @IBOutlet weak private var vehicleTypeIdLabel: UILabel!
    @IBOutlet weak private var pricingParkingLabel: UILabel!
    @IBOutlet weak private var pricingTimeLabel: UILabel!
    @IBOutlet weak private var isActiveHardwareLabel: UILabel!
    @IBOutlet weak private var locationIdLabel: UILabel!
    @IBOutlet weak private var addressLabel: UILabel!
    @IBOutlet weak private var zipcodeLabel: UILabel!
    @IBOutlet weak private var cityLabel: UILabel!
    @IBOutlet weak private var latnLongLabel: UILabel!
    @IBOutlet weak private var reservationStateLabel: UILabel!
    @IBOutlet weak private var damageDescLabel: UILabel!
    
    // MARK: Properties
    var carId: Int!
    private lazy var carDetailsViewModel = CarDetailsViewModel()
    private lazy var quickRentCarViewModel = QuickRentCarViewModel()
    
    // MARK: view Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        getCarDetails()
        
    }
    // getting carDetails from ViewModel
    private func getCarDetails() {
        carDetailsViewModel.getCarDetails(carId: carId) { [weak self] (result) in
            switch result {
            case .success(let carDetails):
                //populating View from car details
                self?.populateView(carDetails)
        
            case .failure(let error):
                //showing error
                self?.showAlert(message: error)
        
            }
        }
    }
    //method to populate View
    private func populateView(_ carDetails: CarDetails) {
        carImageView.setImage(carDetails.vehicleTypeImageURL ?? "")
        titleLabel.text = carDetails.title ?? "N/A"
        licensePlateLabel.text = carDetails.licencePlate ?? "N/A"
        addressLabel.text = carDetails.address ?? "N/A"
        zipcodeLabel.text = carDetails.zipCode ?? "N/A"
        cityLabel.text = carDetails.city ?? "N/A"
        damageDescLabel.text = carDetails.damageDescription ?? "N/A"
        pricingParkingLabel.text = carDetails.pricingParking
        pricingTimeLabel.text = carDetails.pricingTime
        
        if let isClean = carDetails.isClean {
            cleanLabel.text = isClean ? "YES" : "NO"
        }
        if let isDamaged = carDetails.isDamaged {
            cleanLabel.text = isDamaged ? "YES" : "NO"
        }
        if let fuelLevel = carDetails.fuelLevel {
            fuelLevelLabel.text = "\(fuelLevel)"
        }
        if let vehicleStateID = carDetails.vehicleStateID {
            vehicleStateIdLabel.text = "\(vehicleStateID)"
        }
        if let isActiveHardware = carDetails.isActivatedByHardware {
             isActiveHardwareLabel.text = isActiveHardware ? "YES" : "NO"
        }
        if let locationId = carDetails.locationID {
            locationIdLabel.text = "\(locationId)"
        }
        if let lat = carDetails.lat , let long = carDetails.lon {
            latnLongLabel.text = "\(lat), \(long)"
        }
        if let reservationState  = carDetails.reservationState {
            reservationStateLabel.text = "\(reservationState)"
        }
    }
    //quickRent Button Action
    @IBAction private func quickRentButtonPressed(_ sender: UIButton) {
        quickRentCarViewModel.quickRent(carId: carId) { [weak self] (result) in
            switch result {
            case .success(let successMessage):
                self?.showAlert(title: "Reservation Successful", message: successMessage)
            case .failure(let error):
                self?.showAlert(message: error)
            }
        }
    }
    
}
