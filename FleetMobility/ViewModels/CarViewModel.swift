//
//  CarViewModel.swift
//  FleetMobility
//
//  Created by Shayan Ali on 11/09/2019.
//  Copyright © 2019 Shayan Ali. All rights reserved.
//

import Foundation
import MapKit

class  CarViewModel {
    var carList: [Car]!
    weak var delegate: AnnotationsDelegate?
    init(delegate: AnnotationsDelegate) {
        self.delegate = delegate
    }
    
    func getCars(completion: @escaping (APIResult<String>)-> Void) {
        
        Providers.carProvider.request(.getCars) { [weak self] (result) in
            switch result {
            case .success(let response):
                do {
                    let carList : [Car] = try JSONDecoder().decode([Car].self, from: response.data)
                    self?.carList = carList
                    self?.delegate?.getAnnotations(self?.makeAnnotations(carList: carList))
                    completion(.success("Now you can rent cars, please search map"))
                }
                catch {
                    completion(.failure(error.localizedDescription))
                }
               
            case .failure(let error):
                completion(.failure(error.localizedDescription))
                
            }
        }
    }
    //converting Car Objects into annotations
    func makeAnnotations(carList: [Car])-> [CarAnnotation] {
        
        var annotationsList = [CarAnnotation]()
        carList.forEach { (car) in
            let annotation = CarAnnotation(coordinate:CLLocationCoordinate2D(latitude: car.lat ?? 0, longitude: car.lon ?? 0) , carId: car.carID)
            annotation.title = car.title
            annotationsList.append(annotation)
        }
        return annotationsList
    }
}

// protocol for cummunication btw viewModel and MapViewController (passing annotations)
protocol AnnotationsDelegate: class {
    func getAnnotations(_ annotationsList: [CarAnnotation]?)
}
