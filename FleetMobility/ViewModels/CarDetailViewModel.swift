//
//  CarDetailViewModel.swift
//  FleetMobility
//
//  Created by Shayan Ali on 11/09/2019.
//  Copyright © 2019 Shayan Ali. All rights reserved.
//

import Foundation

class  CarDetailsViewModel {
    
    func getCarDetails(carId: Int, completion: @escaping (APIResult<CarDetails>)-> Void) {
        
        Providers.carProvider.request(.getCarDetails(id: carId)) {  (result) in
            switch result {
            case .success(let response):
                do {
                    let carDetails : CarDetails = try JSONDecoder().decode(CarDetails.self, from: response.data)
                  
                    completion(.success(carDetails))
                    
                }
                catch {
                    completion(.failure(error.localizedDescription))
                }
                
            case .failure(let error):
                completion(.failure(error.localizedDescription))
                
            }
        }
    }
    
}
