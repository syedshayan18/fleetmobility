//
//  QuickRentCarViewModel.swift
//  FleetMobility
//
//  Created by Shayan Ali on 11/09/2019.
//  Copyright © 2019 Shayan Ali. All rights reserved.
//

import Foundation

class  QuickRentCarViewModel {
    
    func quickRent(carId: Int, completion: @escaping (APIResult<String>)-> Void) {
        
        Providers.carProvider.request(.quickRentCar(carId: carId)) {  (result) in
            switch result {
            case .success(let response):
                do {
                    let quickRentObj : QuickRentCarResponse = try JSONDecoder().decode(QuickRentCarResponse.self, from: response.data)
                    
                    if let reservationId = quickRentObj.reservationID, response.statusCode == 200 {
                        completion(.success("Vehicle reserved, your reservation ID is \(reservationId)"))
                    }
                    else {
                        completion(.failure("Oops, Something went wrong, please try again"))
                    }
            
                }
                catch {
                    completion(.failure(error.localizedDescription))
                }
                
            case .failure(let error):
                completion(.failure(error.localizedDescription))
                
            }
        }
    }
    
}
