//
//  Extension+UIImage.swift
//  FleetMobility
//
//  Created by Shayan on 9/12/19.
//  Copyright © 2019 Shayan Ali. All rights reserved.
//

import UIKit
import SDWebImage
extension UIImageView {
    
    func setImage(_ url: String) {
        self.sd_setImage(with: URL(string: url), placeholderImage: UIImage(named: "placeholder"))
    }
    
 
}
