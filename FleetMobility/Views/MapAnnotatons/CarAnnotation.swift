//
//  CarAnnotationView.swift
//  FleetMobility
//
//  Created by Shayan on 9/12/19.
//  Copyright © 2019 Shayan Ali. All rights reserved.
//
import Foundation
import MapKit
class CarAnnotation: NSObject, MKAnnotation {
    
    var coordinate: CLLocationCoordinate2D
    var title: String?
    var carId: Int
    
    init(coordinate: CLLocationCoordinate2D, carId: Int) {
        self.coordinate = coordinate
        self.carId = carId
    }
}
