//
//  Car.swift
//  FleetMobility
//
//  Created by Shayan Ali on 11/09/2019.
//  Copyright © 2019 Shayan Ali. All rights reserved.
//
import Foundation

// MARK: - Car
struct Car: Codable {
    let carID: Int
    let title: String?
    let lat, lon: Double?
    let licencePlate: String?
    let fuelLevel, vehicleStateID, vehicleTypeID: Int?
    let pricingTime, pricingParking: String?
    let reservationState: Int?
    let isClean, isDamaged: Bool?
    let distance, address, zipCode, city: String?
    let locationID: Int?
    
    enum CodingKeys: String, CodingKey {
        case carID = "carId"
        case title, lat, lon, licencePlate, fuelLevel
        case vehicleStateID = "vehicleStateId"
        case vehicleTypeID = "vehicleTypeId"
        case pricingTime, pricingParking, reservationState, isClean, isDamaged, distance, address, zipCode, city
        case locationID = "locationId"
    }
}
