//
//  CarDetail.swift
//  FleetMobility
//
//  Created by Shayan Ali on 11/09/2019.
//  Copyright © 2019 Shayan Ali. All rights reserved.
//

import Foundation

// MARK: - CarDetail
struct CarDetails: Codable {
    let carID: Int
    let title: String?
    let isClean, isDamaged: Bool?
    let licencePlate: String?
    let fuelLevel, vehicleStateID: Int?
    let hardwareID: String?
    let vehicleTypeID: Int?
    let pricingTime, pricingParking: String?
    let isActivatedByHardware: Bool?
    let locationID: Int?
    let address, zipCode, city: String?
    let lat, lon: Double?
    let reservationState: Int?
    let damageDescription: String?
    let vehicleTypeImageURL: String?
    
    enum CodingKeys: String, CodingKey {
        case carID = "carId"
        case title, isClean, isDamaged, licencePlate, fuelLevel
        case vehicleStateID = "vehicleStateId"
        case hardwareID = "hardwareId"
        case vehicleTypeID = "vehicleTypeId"
        case pricingTime, pricingParking, isActivatedByHardware
        case locationID = "locationId"
        case address, zipCode, city, lat, lon, reservationState, damageDescription
        case vehicleTypeImageURL = "vehicleTypeImageUrl"
    }
}
