//
//  QuickRentCarResponse.swift
//  FleetMobility
//
//  Created by Shayan on 9/12/19.
//  Copyright © 2019 Shayan Ali. All rights reserved.
//

import Foundation


// MARK: - QuickRentCarResponse
struct QuickRentCarResponse: Codable {
    let reservationID, carID, cost, drivenDistance: Int?
    let licencePlate, startAddress: String?
    let userID: Int?
    let isParkModeEnabled: Bool?
    let damageDescription, fuelCardPin: String?
    let endTime, startTime: Int?
    
    enum CodingKeys: String, CodingKey {
        case reservationID = "reservationId"
        case carID = "carId"
        case cost, drivenDistance, licencePlate, startAddress
        case userID = "userId"
        case isParkModeEnabled, damageDescription, fuelCardPin, endTime, startTime
    }
}
